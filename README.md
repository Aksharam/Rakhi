Rakhi (രാഖി)
==========
ഏഷ്യാനെറ്റിലെ സോഷ്യൽ മീഡിയാകോഡിനേറ്ററായിരുന്ന രാഹുൽ വിജയ് തയ്യാറാക്കിയ മലയാളം യുണീക്കോഡ് അക്ഷരരൂപം. [കൗമുദി](https://github.com/rahul-v/Kaumudi) ഫോണ്ടിന്റെ പഴയലിപി. 

Original Source : https://github.com/rahul-v/Rakhi

